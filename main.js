var bldgLayers = [];
var selectedLayerId;
var contents;
var currentFloor = 1;
var map;


require([
        "esri/request",
        "esri/config",
        "esri/map",
        "esri/tasks/query",
        "esri/tasks/QueryTask",
        "esri/tasks/GeometryService",
        "dojo/dom-construct",
        "esri/layers/GraphicsLayer",
        "esri/layers/FeatureLayer",
        "esri/layers/ArcGISTiledMapServiceLayer", "esri/layers/ArcGISDynamicMapServiceLayer", "esri/layers/LabelClass",
        "esri/graphic",
        "esri/symbols/SimpleLineSymbol",
        "esri/symbols/SimpleFillSymbol",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/symbols/PictureMarkerSymbol",
        "esri/symbols/TextSymbol",
        "esri/renderers/SimpleRenderer",
        "esri/Color", 
        "esri/InfoTemplate",
        "dojo/dom-style",
        "esri/lang", "dojo/parser",
        "dojo/Deferred", "dojo/_base/json",
        "dojo/domReady!"
], function (esriRequest, esriConfig, Map, Query, QueryTask, GeometryService, domConstruct,
        GraphicsLayer,
        FeatureLayer,
        ArcGISTiledMapServiceLayer,
        ArcGISDynamicMapServiceLayer, LabelClass,
        Graphic,
        SimpleLineSymbol,
        SimpleFillSymbol,
        SimpleMarkerSymbol,
        PictureMarkerSymbol,
        TextSymbol,
        SimpleRenderer,
        Color,
        InfoTemplate,
        domStyle,
        esriLang,parser, dojoJSON,
        Deferred
        ) {

    // Allow for CORS with Webservice
    esriConfig.defaults.io.corsEnabledServers.push("http://128.196.84.194:2016");    

    // Map Object
    map = new Map("map", {  
        center: [-110.953, 32.232], // long, lat of UA Old main
        zoom: 18, 
        logo: false,
        smartNavigation: false,
		isZoomSlider: true       
    });      

    // Clears Selection
    map.on("click", function(evt) {
        if (!evt.graphic) {
            hideRoomDetails();
        }
    });

    map.on("extent-change", function(evt) {
    	console.log("Zoom: " + map.getZoom() + " Center: " + map.extent.getCenter().x + "," + map.extent.getCenter().y);
    });

    // Add myTheme CSS to the popup class
    require(["dojo/dom-class"], function(domClass){
    domClass.add(map.infoWindow.domNode, "myTheme");
	});

    // Add Basemap from UA Enterprise GIS
    var customBasemap = new esri.layers.ArcGISTiledMapServiceLayer("http://services.maps.arizona.edu/pdc/rest/services/enterprise/MapServer/");
    map.addLayer(customBasemap);   

     //Transparent Renderer.
    var sfs = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
        new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
            new Color([45, 80, 225, 0.0]), 0), //line color, transparancy, width
        new Color([45, 80, 225, 0.0])); //fill color, transparancy
    var rendererTransparent = new SimpleRenderer(sfs);

    // This URL is for around 8 CALS Buildings, BetaGIS2 is for AHSC
    //var roomsurl = "http://apps.cals.arizona.edu/gis/rest/services/interiorGIS/MapServer/";
    var roomsurl = "http://pdc-betagis2.catnet.arizona.edu:6080/arcgis/rest/services/FMBeta/MapServer/";
    allLayers = new ArcGISDynamicMapServiceLayer(roomsurl, {
        id: "allLayers"
    });

    // Setup InfoTemplate 
    var template = new InfoTemplate();
    template.setTitle("Using EGIS to Access External Data");
    template.setContent(showDetails); 

    // Setup Clientside Labeling
    var label = new TextSymbol();
    label.font.setSize("10pt");
    label.font.setFamily("arial");
    var labelJson = { "labelExpressionInfo": {"value": "{RM_ID}"} };
    var labelClass = new LabelClass(labelJson);
    labelClass.symbol = label;
    labelClass.maxScale = 20;
    labelClass.minScale = 21;

    allLayers.on("load", function() {
        var layerInfos = map.getLayer("allLayers").layerInfos;
        for (i = 0; i < layerInfos.length; i++) {
            var fc;
            //console.log(layerInfos[i].name);
            bldgLayers.push(layerInfos[i].name);
            
            fc = new FeatureLayer(roomsurl + i, {
                id: layerInfos[i].name,
                mode: FeatureLayer.MODE_SNAPSHOT,
                visible: layerInfos[i].defaultVisibility,
                infoTemplate: template,
                outFields: ["*"]
            });

			// This allows user to see through feature class down to dynamic layer.
			fc.setRenderer(rendererTransparent);  

			// Client Side Labeling
			fc.setLabelingInfo([labelClass]);

			// Add Featurelayer to map
			map.addLayer(fc);

            /*fc.on("click", function(evt) {
                showRoomDetails(evt.graphic);
            }); */            

            // Experiments in Querying Service.  Highlight Classrooms.
			/*if (fc.visible == true) {
               setupQuery(fc.url);
            }*/

        } // end for loop

        // Set Visibility to First Floor
        updateLayerVisibility(1);

    }); //end allLayers.on()
    map.addLayer(allLayers);    

    // Resize default infowindow
    map.infoWindow.resize(600, 400);   

    //selected color
    var sfs2 = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
        new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
            new Color([255, 255, 255, 1]), 2),
        new Color([39, 101, 232, .8]));
    var rendererSelected = new SimpleRenderer(sfs2);

    var selectedFeatures = new GraphicsLayer({
        id: "selectedFeatures"
    });

    selectedFeatures.setRenderer(rendererSelected);
    map.addLayer(selectedFeatures);

    // This function pulls data from the feature class, makes an async call to the PDC Room API, renders a bunch of HTML for the infowindow and
    // returns formatted results to the Infowindow for Display
    function showDetails(graphic, _selectedLayerId) {

        //clear previously selected room.
        map.getLayer("selectedFeatures").clear();

        //shade selected polygon
        map.getLayer("selectedFeatures").add(new Graphic(graphic.geometry));

        var selectedBldg = graphic.attributes["ROOMEXT.BL_ID"]
        //selectedBldg = parseInt(selectedBldg, 10);
        var selectedFloor= graphic.attributes["ROOMEXT.FL_ID"]
        //selectedFloor = parseInt(selectedFloor, 10);
        var selectedRoom = graphic.attributes["ROOMEXT.RM_ID"]
        //selectedRoom = parseInt(selectedRoom, 10);

        //console.log("Vars: " + selectedBldg+"+"+selectedFloor+"+"+selectedRoom)

        var roomURL = 'http://128.196.84.194:2016/api/room/epm_room2?buildingID='+selectedBldg+'&floorID='+selectedFloor+'&roomID='+selectedRoom
        
        // Call Webservice using Bldg, Floor, Room as key to obtain external data
		var request = esri.request({
		url: roomURL,
		handleAs: "json"
		});
		request.then(requestSucceeded, requestFailed);
		
		function requestSucceeded(data) {
 			//console.log("Data: ", data); // print the data to browser's console
 			var roomJson = JSON.parse(data);
                if (roomJson[0] == undefined) { 
                    OfficialOrg = "<font color=red><b>Not Provided in XML</b></b>"
                } else {
                    console.log(roomJson[0].OFFICIAL_ORG + " in success..!");
                    var OfficialOrg = roomJson[0].OFFICIAL_ORG;
                    var BldgName = roomJson[0].NAME_BL;
                    var Sqft = roomJson[0].AREA;
                    var Desc = roomJson[0].RM_USE_DESCRIPTION;

				    $('.contentPane').html($('.contentPane').html().replace("DeptName", OfficialOrg));
				    $('.contentPane').html($('.contentPane').html().replace("BldgName", BldgName));
				    $('.contentPane').html($('.contentPane').html().replace("RMSqFt", Sqft + " sqft"));
				    $('.contentPane').html($('.contentPane').html().replace("RMDesc", Desc));
				    
            }
 			//console.log("Org: ", OfficialOrg)
		}

		function requestFailed(error) {
			console.log("Error: ", error.message);
		}

		// Use of this backtick makes pasting HTML very convenient. Does not work in IE however.
	    contents = `
	    <div class="w-clearfix insertcontent">
          <div class="infopane rightinfo">
            <div class="infopanetitle">Data From<br>External Service</div>
            <div class="infodatatable">
              <div class="w-clearfix infodatarow altcolor">
                <div class="infodatatablecol1">
                  <div>Bldg Name:</div>
                </div>
                <div class="infodatatablecol2">
                  <div>BldgName</div>
                </div>
              </div>
              <div class="w-clearfix infodatarow">
                <div class="infodatatablecol1">
                  <div>Department:</div>
                </div>
                <div class="infodatatablecol2">
                  <div>DeptName</div>
                </div>
              </div>
              <div class="w-clearfix infodatarow altcolor">
                <div class="infodatatablecol1">
                  <div>Area:</div>
                </div>
                <div class="infodatatablecol2">
                  <div>RMSqFt</div>
                </div>
              </div>
              <div class="w-clearfix infodatarow">
                <div class="infodatatablecol1">
                  <div>Description:</div>
                </div>
                <div class="infodatatablecol2">
                  <div>RMDesc</div>
                </div>
              </div>
            </div>
          </div>
          <div class="infopane leftinfo">
            <div class="infopanetitle">Data From<br>Enterprise GIS</div>
            <div class="infodatatable">
              <div class="w-clearfix infodatarow altcolor">
                <div class="infodatatablecol1">
                  <div>Bldg No:</div>
                </div>
                <div class="infodatatablecol2">
                  <div>${selectedBldg}</div>
                </div>
              </div>
              <div class="w-clearfix infodatarow">
                <div class="infodatatablecol1">
                  <div>Floor:</div>
                </div>
                <div class="infodatatablecol2">
                  <div>${selectedFloor}</div>
                </div>
              </div>
              <div class="w-clearfix infodatarow altcolor">
                <div class="infodatatablecol1">
                  <div>Room:</div>
                </div>
                <div class="infodatatablecol2">
                  <div>${selectedRoom}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      `
        return contents;
    }  

    // Query to Highlight Offices (RM_CAT=130)
    function setupQuery (queryurl) {
            var queryTask = new QueryTask(queryurl);

            //build query filter
            var query = new Query();
            query.returnGeometry = true;
            //query.outFields = ["RM_CAT_DESCRIPTION"];
            //query.outSpatialReference = { "wkid": 102100 };
            query.where = "ROOM.RM_CAT = 310";

            //Can listen for complete event to process results
            //or can use the callback option in the queryTask.execute method.
            queryTask.on("complete", function (event) {
              //map.graphics.clear();
              var highlightSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                  new Color([255, 0, 0]), 3), new Color([125, 125, 125, 0.35]));

              var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                  new Color([255, 255, 255, 0.35]), 1), new Color([255, 0, 0, 1]));

              var features = event.featureSet.features;

              var queryGraphicsLayer = new GraphicsLayer();
              //QueryTask returns a featureSet.
              //Loop through features in the featureSet and add them to the map.
              var featureCount = features.length;
              for (var i = 0; i < featureCount; i++) {
                //Get the current feature from the featureSet.
                var graphic = features[i]; //Feature is a graphic
                graphic.setSymbol(symbol);
               
                queryGraphicsLayer.add(graphic);
              }

              map.addLayer(queryGraphicsLayer);
            });

            queryTask.execute(query);
    }  

$(".upbutton").click(function(){
    currentFloor++;
    updateLayerVisibility(currentFloor)
});

$(".downbutton").click(function(){
    currentFloor--;
    updateLayerVisibility(currentFloor)
});

function updateLayerVisibility(currentFloor) {
    map.infoWindow.hide()
    var visibleLayerIds = [];
    console.log("Current Floor: " + currentFloor);
    $(".floorindicator").html(currentFloor);
    var layerInfos = map.getLayer("allLayers").layerInfos;
    for (i = 0; i < layerInfos.length; i++) {
            //console.log(layerInfos[i].name);
            if (layerInfos[i].name.substr(layerInfos[i].name.length - 1) == currentFloor) {
                visibleLayerIds.push(i)
            }
    }
    //console.log(visibleLayerIds);
    allLayers.setVisibleLayers(visibleLayerIds);

    // Loop through Graphics Layers
    for (i=0; i < map.graphicsLayerIds.length; i++) {
        fc = map.getLayer(map.graphicsLayerIds[i])
        if (fc.id.substr(fc.id.length - 1) == currentFloor) {
                fc.show();
                //console.log("Show: " + fc.id)
            } else {
                fc.hide();
                //console.log("Hide: " + fc.id)
            }
        
    }
}


});

function hideRoomDetails() {
    map.getLayer("selectedFeatures").clear();
}



